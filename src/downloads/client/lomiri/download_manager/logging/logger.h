/*
 * Copyright 2014 Canonical Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 3 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef LOMIRI_DOWNLOADMANAGER_CLIENT_LOGGER_H
#define LOMIRI_DOWNLOADMANAGER_CLIENT_LOGGER_H

#include <QMap>
#include <QMutex>
#include <QString>
#include <QStringList>
#include <QVariant>


namespace Lomiri {

namespace DownloadManager {

class DownloadImpl;
class DownloadPCW;
class ManagerImpl;
class DownloadManagerPCW;
class DownloadsListManagerPCW;
class GroupManagerPCW;
class MetadataDownloadsListManagerPCW;
class DownloadStruct;

namespace Logging {

class LoggerPrivate;

/*!
    \class Logger
    \brief The Logger class allows to control a logging that
           is performed within the library allowed the developer
           to set the log level as well as the path of the file
           where the logs are written.
    \since 0.5

*/
class Logger {
    friend class Lomiri::DownloadManager::DownloadImpl;
    friend class Lomiri::DownloadManager::DownloadPCW;
    friend class Lomiri::DownloadManager::ManagerImpl;
    friend class Lomiri::DownloadManager::DownloadManagerPCW;
    friend class Lomiri::DownloadManager::DownloadsListManagerPCW;
    friend class Lomiri::DownloadManager::GroupManagerPCW;
    friend class Lomiri::DownloadManager::MetadataDownloadsListManagerPCW;

 public:
    /*! The different log levels supported by the default logger of the lib. */
    enum Level
    {
        Debug,
        Normal,
        Notification,
        Warning,
        Error,
        Critical
    };

    /*!
        \fn static void Logger::init(Level lvl, const QString& path)

        Initializes the logging service for the download library using
        the given \a level and writing the logs to the given \a path.
    */
    static void init(Level lvl, const QString& path);

 protected:

    /*! \cond PRIVATE
     */
    static void log(Level lvl, const QString& msg);
    static void log(Level lvl, const QStringList& msg);
    static void log(Level lvl, const QString& msg, QMap<QString, QString> map);
    static void log(Level lvl, const QString& msg, QMap<QString, QVariant> map);
    static void log(Level lvl, const QString& msg, DownloadStruct downStruct);
    /*! \endcond
     */

 private:
    static QMutex _mutex;
    static LoggerPrivate* _private;
};

}  // Logging

}  // DownloadManager

}  // Lomiri

#endif
