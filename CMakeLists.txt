cmake_minimum_required(VERSION 3.5)

project(lomiri-download-manager VERSION 0.1.3 LANGUAGES CXX)

option(ENABLE_UBUNTU_COMPAT "Enable Ubuntu.DownloadManager QML compatibility layer" OFF)
option(ENABLE_DOC "Build documentation" ON)
option(ENABLE_WERROR "Treat all build warnings as errors" ON)
option(USE_SYSTEMD "Install systemd service" ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -fPIC -pthread")

# for dh_translations to extract the domain
# (regarding syntax consistency, see http://pad.lv/1181187)
set (GETTEXT_PACKAGE "lomiri-download-manager")
add_definitions(-DI18N_DOMAIN="${GETTEXT_PACKAGE}")

# Standard install paths
include(GNUInstallDirs)

find_package(CoverageReport)
#####################################################################
# Enable code coverage calculation with gcov/gcovr/lcov
# Usage:
#  * Switch build type to coverage (use ccmake or cmake-gui)
#  * Invoke make, make test, make coverage
#  * Find html report in subdir coveragereport
#  * Find xml report feasible for jenkins in coverage.xml
#####################################################################
IF(CMAKE_BUILD_TYPE MATCHES [cC][oO][vV][eE][rR][aA][gG][eE])
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ftest-coverage -fprofile-arcs" )
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ftest-coverage -fprofile-arcs" )
  SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -coverage" )
  SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -coverage" )
  ENABLE_COVERAGE_REPORT(TARGETS ${DIALER_APP})
ENDIF(CMAKE_BUILD_TYPE MATCHES [cC][oO][vV][eE][rR][aA][gG][eE])

set(QT_INSTALL_QML "${CMAKE_INSTALL_FULL_LIBDIR}/qt5/qml")

set(LDM_VERSION_MAJOR 0)
set(LDM_VERSION_MINOR 1)
set(LDM_VERSION_PATCH 2)

file(GLOB_RECURSE I18N_CPP_SRC_FILES
     RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
     src/**.cpp)
list(SORT I18N_CPP_SRC_FILES)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Core DBus Network Sql Test REQUIRED)
find_package(Boost COMPONENTS log program_options REQUIRED)
find_package(glog REQUIRED)

find_package(PkgConfig REQUIRED)
pkg_check_modules(DBUS REQUIRED dbus-1)
pkg_check_modules(LOMIRI_API REQUIRED liblomiri-api)
pkg_check_modules(APPARMOR REQUIRED libapparmor)

if(USE_SYSTEMD)
  pkg_check_modules(SYSTEMD systemd)
  if (NOT SYSTEMD_FOUND)
    message(FATAL_ERROR "systemd pkg-config not found (required for figuring out service install location)")
  endif()

  pkg_get_variable(SYSTEMD_USER_DIR systemd systemduserunitdir)
endif()

include(CTest)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pipe -std=c++17 -O2 -Wall -W -D_REENTRANT -fPIC -pedantic -Wextra")
add_definitions("-DNDEBUG")

if(ENABLE_WERROR)
  add_compile_options("-Werror")

  # We make use of QNetworkConfiguration{,Manager} which are deprecated in
  # Qt 5.15, yet the replacement is available in Qt 6.1 and up. I don't feel
  # like its worth it looking into replacement now, lets turn it to
  # non-fatal warnings.
  add_compile_options("-Wno-error=deprecated-declarations")
endif()

if(NOT CMAKE_BUILD_TYPE)
	message(STATUS "Setting build type to 'RelWithDebInfo' as none was specified.")
	set(CMAKE_BUILD_TYPE RelWithDebInfo)
endif()

string(TOLOWER "${CMAKE_BUILD_TYPE}" cmake_build_type_lower)

if(cmake_build_type_lower MATCHES relwithdebinfo) # workaround for http://pub    lic.kitware.com/Bug/view.php?id=14696
	add_definitions(-DQT_NO_DEBUG)
endif()

add_subdirectory(src)
if(BUILD_TESTING)
  add_subdirectory(tests)
endif()
if(ENABLE_DOC)
  add_subdirectory(docs)
endif(ENABLE_DOC)
add_subdirectory(po)
